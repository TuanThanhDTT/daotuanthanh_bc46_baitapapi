
function fetchStudentList(){
    batLoading();
    listSvServ.getList()
    .then(function(res){
        dssv = res.data;
        renderDSSV(dssv);
        tatLoading();
    }).catch(function(err){
        tatLoading();
    })
}
fetchStudentList();

//Xóa sinh viên

function xoaSinhVien(idSV){
    batLoading();
    listSvServ.delete(idSV).then((res) => {
            fetchStudentList();
            tatLoading();
          })
          .catch((err) => {
           tatLoading();
          });
}

//Thêm sinh viên

function themSinhVien(){
    batLoading();
    let newStudent = layThongTinTuForm();
    listSvServ.create(newStudent).then((res) => {
            fetchStudentList();
            tatLoading();
          })
          .catch((err) => {
           tatLoading();
          });
}

//Sửa thông tin sinh viên
let idUpdateSV = null;
function suaSinhVien(idSV){
    idUpdateSV = idSV;
    batLoading();
    //Gọi API lấy chi tiết data thông tin sinh viên có id trùng với id nút sửa nút sửa đem xuống
    listSvServ.getItemByID(idSV).then((res) => {
            showThongTinLenForm(res.data);
            tatLoading();
          })
          .catch((err) => {
           tatLoading();
          });
}

function capNhatSinhVien(){
    batLoading();
    let studentItem = layThongTinTuForm();
    listSvServ.update(idUpdateSV, studentItem).then((res) => {
            fetchStudentList();
          })
          .catch((err) => {
          });
}








