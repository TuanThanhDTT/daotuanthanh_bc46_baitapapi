//Chức năng lấy item trong array dssv render lên màn hình
function renderDSSV(studentList){
    var contentHTML = "";
    for(var i = studentList.length -1; i >= 0; i--){
        var item_sv = studentList[i];
        var diemTB = ((parseFloat(item_sv.diemToan) +  parseFloat(item_sv.diemLy) + parseFloat(item_sv.diemHoa)) / 3).toFixed(2);
        var content = `<tr>
            <td>${item_sv.id}</td>
            <td id="tenSv">${item_sv.name}</td>
            <td id="mailSv">${item_sv.email}</td>
            <td >${diemTB}</td>
            <td>
                <button onclick="xoaSinhVien('${item_sv.id}')" class="btn btn-danger">Xóa</button>
                <button onclick="suaSinhVien('${item_sv.id}')" class="btn btn-warning">Sửa</button>
            </td>
        </tr>`
        contentHTML = contentHTML + content;
    }
    document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

function batLoading(){
    document.getElementById("spinner").style.display = "flex";
}

function tatLoading(){
    document.getElementById("spinner").style.display = "none";
} 
//Chức năng lấy thông tin từ form
function layThongTinTuForm(){
        //lấy giá trị user nhập từ form
        var id = document.querySelector("#txtMaSV").value;
        var ten = document.getElementById("txtTenSV").value;
        var email = document.getElementById("txtEmail").value;
        var matKhau = document.querySelector("#txtPass").value;
        var toan = document.getElementById("txtDiemToan").value*1;
        var ly = document.getElementById("txtDiemLy").value*1;
        var hoa = document.getElementById("txtDiemHoa").value*1;
        //truyền giá trị vào lớp đối tượng SinhVien trong file model.js , có thể hiểu là gọi lại hàm Sinh Vien đã được định nghĩa ở file model.js và truyền giá trị người dùng nhập vào.
        return {
            name: ten,
            email: email,
            matKhau: matKhau,
            diemToan: toan,
            diemLy: ly,
            diemHoa: hoa,
        }
}

//Show thông tin lên form, đi định nghĩa hàm sẽ thực thi khi gọi lại và truyền giá trị là đối tượng tại index cho tham số của function. 
function showThongTinLenForm(studentList){
    document.querySelector("#txtMaSV").value = studentList.id;
    document.getElementById("txtTenSV").value = studentList.name;
    document.getElementById("txtEmail").value = studentList.email;
    document.querySelector("#txtPass").value = studentList.matKhau;
    document.getElementById("txtDiemToan").value = studentList.diemToan;
    document.getElementById("txtDiemLy").value = studentList.diemLy;
    document.getElementById("txtDiemHoa").value = studentList.diemHoa;
}

