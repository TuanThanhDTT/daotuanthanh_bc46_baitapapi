const BASE_URL = "https://647f229ec246f166da902468.mockapi.io/studentlist";
var listSvServ = {
    getList: () => {
        return axios({
            url: BASE_URL,
            method: "GET",
        });
    },
    delete: (id) => {
        return axios({
            url: `${BASE_URL}/${id}`,
            method: "DELETE",
        });
    },
    create: (newStudent) => {
        return axios({
            url: BASE_URL,
            method: "POST",
            data: newStudent,
        });
    },
    getItemByID: (id) => {
        return axios({
            url: `${BASE_URL}/${id}`,
            method: "GET",
        });
    },
    update: (id, studentItem) => {
        return axios({
            url: `${BASE_URL}/${id}`,
            method: "PUT",
            data: studentItem,
        });
    },
}